## Project summary

The project's scope is to automatize repetitive configuration changes.
This version automatize the following configuration changes on a Fabricpath deployment with Cisco Nexus devices:

* New vrf creation
* New subnet creation.
* New ospf configurarion and a L3out towards a Firewall.

## Files

Ansible network Automation.pdf:
This is the target architecture to automate configuration changes.

Tests folder:
Automated tests done in the Gilab CI/CD pipeline.

inputTemplates
Templates for the input variables for each configuration change.


## How to install it.

You can have a look at .gitlab-ci.yml. In this file it is automated the process of installing the package in a linux VM.
Copy the folder "ansible" to the /etc/ folder on your Linux machine. I have tested this in Ubuntu.

## How to run it.

Please have a look at .gitlab-ci.yml. There are 3 tests available.

* Create a new vrf:

ansible-playbook /etc/ansible/playbooks/config_newVrf_fp.yml --extra-vars  "@/path/to/inputVariables"

* Configure ospf and L3out towards a FW:

ansible-playbook /etc/ansible/playbooks/config_newOspfL3Out_dsFw_fp.yml --extra-vars  "@/path/to/inputVariables"

* Configure a new subnet:

ansible-playbook /etc/ansible/playbooks/config_newNetwork_fp.yml --extra-vars  "@/path/to/inputVariables"



